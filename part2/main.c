
/* -----------------------------------Project Description--------------------------------
 *Author: Linyi Fu
 *Description: This is project first part and use circle ring buffer to alloc the data enqueue and dequeue.
 *Date: Nov/28.2019
 */


/*-----------------------------------Head File ----------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include <pthread.h>

#define MAXENTRIES 3
#define MAXNAME 16
#define URLSIZE 16
#define CAPSIZE 16
#define MAXQUEUE 3
#define NUMPROXIES 3
/*-----------------------------------Main Function --------------------------------*/

pthread_mutex_t mutex[MAXQUEUE]= PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t  condition_cond  = PTHREAD_COND_INITIALIZER;
pthread_mutex_t condition_mutex = PTHREAD_MUTEX_INITIALIZER;


int counter[MAXQUEUE];

struct topicEntry{
    int entryNum;
    struct timeval timeStamp;
    int pubID;
    char photoURL[URLSIZE];
    char photoCaption[CAPSIZE];
};


struct CRB{
    struct topicEntry *entry;
    int head;
    int tail;
    int length; ;
};

struct combineArg{
    int order;
    struct CRB *crb;
    struct topicEntry *entry;
    int lastEntry;
};

struct thread_pool{
    pthread_t pool[NUMPROXIES];
    int flag[NUMPROXIES];
    int thread_id[NUMPROXIES];
};

int enqueue(struct CRB *crb, struct topicEntry *entry){
    if((crb -> head) == (crb -> tail )){
        printf(" queue is FULL\n");
        return 0;
    }
    else{
        if (crb -> tail == -1){
            (crb -> tail) ++;
        }
        gettimeofday(&(entry -> timeStamp), NULL);
//        entry -> entryNum = head +1;
        crb -> entry[crb -> head].entryNum = entry->entryNum;
        crb -> entry[crb -> head].timeStamp.tv_sec = entry->timeStamp.tv_sec;
        crb -> entry[crb -> head].pubID = entry->pubID;
        strcpy(crb -> entry[crb -> head].photoURL , entry->photoURL);
        strcpy(crb -> entry[crb -> head].photoCaption , entry->photoCaption);


        (crb -> head )++;
        
        if((crb -> head) == (crb -> length)) {
            crb -> head = 0;
        }
        return 1;
    }
}

int getEntry(int lastEntry, struct CRB *crb, struct topicEntry *empty){
    if((crb -> tail == -1)&&(crb -> head == 0)){
        printf("The Queue is empty\n");
        return 0;
    }
    else{
        int check = 1;
        for (int k = 0; k< (crb -> length); k ++){
            if (lastEntry + 1  == crb->entry[k].entryNum){
                empty ->entryNum = crb->entry[k].entryNum;
                empty ->timeStamp = crb->entry[k].timeStamp;
                empty ->pubID = crb->entry[k].pubID;
                strcpy(empty ->photoURL, crb->entry[k].photoURL);
                strcpy(empty ->photoCaption, crb->entry[k].photoCaption);
                return 1;
            }
            if ((lastEntry + 1  > crb->entry[k].entryNum)&&(crb->entry[k].entryNum != -1)){
                check  = check && 1;
            }
            else {check = check&&0;}
        }
        if(check){
            printf("All enries in the queue are less than input.\n");
            return 0;
        }
        for (int k = 0; k< (crb -> length); k ++){
            if (lastEntry + 1  < crb->entry[k].entryNum){
                empty ->entryNum = crb->entry[k].entryNum;
                empty ->timeStamp = crb->entry[k].timeStamp;
                empty ->pubID = crb->entry[k].pubID;
                strcpy(empty ->photoURL, crb->entry[k].photoURL);
                strcpy(empty ->photoCaption, crb->entry[k].photoCaption);
                return crb -> entry[k].entryNum;
            }
        }
        
    }
    return 0;
}

int dequeue (struct timeval delta, struct CRB *crb ){
    if ((crb -> head == 0)&&(crb -> tail == -1)){
        printf("The Queue is empty\n");
        return 0;
    }
    else{
        for (int k = 0; k< (crb -> length);){
            printf("%ld\n", delta.tv_sec);
            printf("%ld\n", crb->entry[k].timeStamp.tv_sec);


            if( delta.tv_sec - crb->entry[k].timeStamp.tv_sec > 2){
                for (int t = k; t <(crb -> length) -1 ; t ++){
                    crb -> entry[t] = (crb -> entry[t+1]);
                }
                crb -> entry[crb->length -1].entryNum = -1;
                crb -> entry[crb->length -1].timeStamp.tv_sec = delta.tv_sec;
                crb -> head --;
                if (crb -> head == -1){crb -> head = (crb -> length -1);}
                if (crb -> entry[0].entryNum == -1){
                    crb -> head = 0;
                    crb -> tail = -1;
                }
            }
            else{
                k++;
            }
        }
        return 1;
    }
}

void *publisher(void *args){
    pthread_mutex_lock(&condition_mutex);
    pthread_cond_wait(&condition_cond, &condition_mutex);
    pthread_mutex_unlock(&condition_mutex);

    

    struct CRB *crb;
    struct topicEntry *entry;
    int order;
    order = ((struct combineArg *) args) -> order;

    crb = ((struct combineArg *) args) -> crb;
    entry =((struct combineArg *) args) -> entry;
    pthread_mutex_lock (&mutex[order]);

//    sleep(1);
    enqueue(crb,entry);
//    printf("args.crb.name: %s\n", crb->name);
//    printf("entry.num.: %d\n", entry->entryNum);
    printf("pub counter[%d]: %d\n",order,counter[order]);
    counter[order]++;
    pthread_mutex_unlock(&mutex[order]);
    return NULL;
};

void *subscrber(void *args){
    pthread_mutex_lock(&condition_mutex);
    pthread_cond_wait(&condition_cond, &condition_mutex);
    pthread_mutex_unlock(&condition_mutex);

    
    

    struct CRB *crb;
    struct topicEntry *entry;
    int lastEntry;
    int order;
    order = ((struct combineArg *) args) -> order;

    
    crb = ((struct combineArg *) args) -> crb;
    entry =((struct combineArg *) args) -> entry;
    lastEntry =((struct combineArg *) args) -> lastEntry;
    pthread_mutex_lock (&mutex[order]);
    getEntry(lastEntry,crb,entry);
    printf(" sub counter[%d]: %d\n",order,counter[order]);
    counter[order]++;
    pthread_mutex_unlock(&mutex[order]);
    return NULL;
};

void *cleanup(void *args){
    int order;
    order = ((struct combineArg *) args) -> order;

    struct CRB *crb;
    struct timeval delta;
    crb = ((struct combineArg *) args) -> crb;
    pthread_mutex_lock (&mutex[order]);

    gettimeofday(&delta,NULL);
    printf("%ld", crb->entry->timeStamp.tv_sec);
    dequeue(delta,crb);
    printf("counter[%d]: %d\n",order,counter[order]);
    counter[order]++;
    pthread_mutex_unlock(&mutex[order]);
    return NULL;

};

void initQueue(struct CRB *crb){
    crb->head = 0;
    crb->tail = -1;
    crb->length = MAXENTRIES;
    crb->entry = malloc (crb->length * sizeof(struct topicEntry));
    for (int k = 0; k < MAXENTRIES;k++){
        crb->entry[k].entryNum = -1;
        gettimeofday(&(crb->entry[k].timeStamp), NULL);
        crb->entry[k].pubID = -1;
        strcpy(crb->entry[k].photoURL, "");
        strcpy(crb->entry[k].photoCaption, "");
    }
}
void initEntry(struct topicEntry *entry){
    entry->entryNum = -1;
    gettimeofday(&(entry->timeStamp), NULL);
    entry->pubID = -1;
    strcpy(entry->photoCaption, "");
    strcpy(entry->photoURL, "");
}

void initComb(struct combineArg *combine){
    combine->crb = NULL;
    combine->entry = NULL;
    combine->lastEntry = -1;
    combine->order = -1;
}

void initPool(struct thread_pool *a){
    for (int i = 0; i<NUMPROXIES; i++){
        a->flag[i] = 0;
        a->thread_id[i] = -1;
    }
}
int main (int argc, char *argv[]){
    //-------init queueArray--------//
    struct CRB crbArray[MAXQUEUE];
    for(int i = 0; i < MAXQUEUE; i ++){
        initQueue(&crbArray[i]);
    }
    // ---------------init entryArray--------------------------------//
    struct topicEntry entryArray[MAXENTRIES * MAXQUEUE];
    for (int i = 0; i<MAXENTRIES * MAXQUEUE; i ++ ){
        initEntry(&entryArray[i]);
    }
    // -----------------init empty entry------------------------------//
    struct topicEntry empty;
    initEntry(&empty);
    
    entryArray[0].entryNum = 1;
    entryArray[1].entryNum = 2;
    entryArray[2].entryNum = 3;
    
    gettimeofday(&(entryArray[0].timeStamp), NULL);
    gettimeofday(&(entryArray[1].timeStamp), NULL);
    gettimeofday(&(entryArray[2].timeStamp), NULL);


//  -----init combineStrut  --------------//
    
    struct combineArg combineArray[MAXENTRIES * MAXQUEUE];
    struct combineArg combineEmpty,combineClean;
    for (int i  = 0; i < MAXENTRIES * MAXQUEUE; i++){
        initComb(&combineArray[i]);
    }
    
    combineArray[0].crb =&crbArray[0];
    combineArray[1].crb =&crbArray[1];
    combineArray[2].crb =&crbArray[2];
    combineArray[3].crb =&crbArray[2];
    combineArray[4].crb =&crbArray[2];
    combineArray[5].crb =&crbArray[2];


    
    combineArray[0].entry =&entryArray[0];
    combineArray[1].entry =&entryArray[1];
    combineArray[2].entry =&entryArray[2];
    combineArray[3].entry =&entryArray[2];
    combineArray[4].entry =&entryArray[2];
    combineArray[5].entry =&entryArray[2];
    
    combineArray[0].order =0;
    combineArray[1].order =1;
    combineArray[2].order =2;
    combineArray[3].order =2;
    combineArray[4].order =2;
    combineArray[5].order =2;


    combineEmpty.crb = &crbArray[2];
    combineEmpty.entry = &empty;
    combineEmpty.lastEntry = 0;
    combineEmpty.order = 0;
    
    combineClean.crb = &crbArray[2];
    combineClean.entry = NULL;
    combineClean.lastEntry = -1;
    combineClean.order = 2;
    


    
    // --------------------------- pthread running ------------------------//
//    pthread_t thread_id[NUMTHREAD];
//
//    pthread_create(&thread_id[0], NULL, publisher,&combineArray[0] );
//    pthread_create(&thread_id[1], NULL, publisher,&combineArray[1] );
//    pthread_create(&thread_id[2], NULL, publisher,&combineArray[2] );
//    pthread_create(&thread_id[3], NULL, publisher,&combineArray[3] );
//    sleep(1);
//    pthread_create(&thread_id[4], NULL, publisher,&combineArray[4] );
//    pthread_create(&thread_id[5], NULL, subscrber,&combineEmpty);
//    sleep(2);
//
//    pthread_create(&thread_id[6], NULL, cleanup,&combineClean);

    struct thread_pool subPool, pubPool;

    initPool(&subPool);
    initPool(&pubPool);

    for (int i = 0; i< NUMPROXIES;i++ ){
        if(pubPool.flag[i]!= 1){
            pthread_create(&pubPool.pool[i],NULL,publisher,&combineArray[i]);
            pubPool.flag[i] = 1;
            pubPool.thread_id[i] = i;
        }
    }
    sleep(1);
    for (int i = 0; i< NUMPROXIES; i++){
        pthread_cond_signal(&condition_cond);
    }
    
    sleep(1);


    
    
    
    for (int i = 0;i < NUMPROXIES; i++) {
        if(subPool.flag[i]!= 1){
            pthread_create(&subPool.pool[i],NULL,subscrber,&combineEmpty);
            subPool.flag[i] = 1;
            subPool.thread_id[i] = i;}
    }
        
    sleep(1);

    
    for (int i = 0; i< NUMPROXIES; i++){
        pthread_cond_signal(&condition_cond);
    }
    
    
    //------------------------------------------

    for (int i = 0; i <NUMPROXIES; i++){
        pthread_join(subPool.pool[i] ,NULL);
        pubPool.flag[i] = 0;
        pthread_join(pubPool.pool[i] ,NULL);
        subPool.flag[i] = 0;

    }

    printf("---------------\n");

    for(int i = 0; i< MAXQUEUE;i++){
        for (int k= 0; k<MAXENTRIES; k++){
            printf("entryNum is: %d\n", crbArray[i].entry[k].entryNum);
        }
        printf("---------------\n");

    }
    
    printf("get answer: %d\n", empty.entryNum);

    // ---------------------------buffer free --------------------//
    
    for (int i = 0; i < MAXQUEUE;i++){
        free(crbArray[i].entry);
    }
    return 1;
    
}

